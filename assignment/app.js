const computersElement = document.getElementById("computers");
const loanButtonElement = document.getElementById("loan");
const totalDueButtonElement = document.getElementById("balance");
const addWorkButttonElement = document.getElementById("work");
const payElement = document.getElementById("paid");
const balanceElement = document.getElementById("balance");
const bankButtonElement = document.getElementById("bank");
const repayButtonElement = document.getElementById("repayloan");
const loanBalanceElement = document.getElementById("loanbalance");
const imageElement = document.getElementById("computer");//
const featuresElement = document.getElementById("specs");//
const descpElement = document.getElementById("descp");//
const priceElement = document.getElementById("price")

let selectedComputer;
let computers = []; //computer
let bank = 0;   
let totalDue = 0.0;
let pay = 0;
let balance = 0;
let loan = 0;
let approvedLoan = 0;
let balanceDiffrence = 0;
let deductTen = 0;
let deductNine = 0;
repayButtonElement.style.display = "none";
loanBalanceElement.style.display = "none";

//fetches computers from API
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then(response => response.json())
  .then(data => computers = data)
  .then(computers => {
    selectComputer(computers[0])
    addComputersToMenu(computers)
  });
// handle the response
const handlerComputerChange = e => {

  selectedComputer = computers[e.target.selectedIndex];
  selectComputer(selectedComputer)

  /*
  console.log(selectedComputer);
  imageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedComputer.image;
  featuresElement.innerText = selectedComputer.specs;
  descpElement.innerText = selectedComputer.description;
  priceElement.innerHTML = `${selectedComputer.price.toFixed(2)}` + " kr";
  */
}

//Displaying infos about laptops
function selectComputer(computer) {

  selectedComputer = computer

  let body = `<div class="card">
      <img src="https://noroff-komputer-store-api.herokuapp.com/${selectedComputer.image}" id="computer" width="42" height="42" style="vertical-align:bottom">
      <h3><span id="title">${selectedComputer.title}</span></h3>
      <p><small id="descp">${selectedComputer.description}</small></p>
      <p><small id="price">${selectedComputer.price} Kr</small></p>
      <button onclick="buttonbuy()" class="buy" id="buy" type="submit">BUY NOW</button>
      </div>`

  document.getElementById("selected_computer").innerHTML = body
  featuresElement.innerHTML = selectedComputer.specs


}



const addComputersToMenu = (computers) => {

  computers.forEach(x => addComputerToMenu(x));

}
//Addding computers for select dropdown options
const addComputerToMenu = (computer) => {

  const computerElement = document.createElement("option");
  computerElement.value = computer.id;
  computerElement.appendChild(document.createTextNode(computer.title));
  computersElement.appendChild(computerElement);
}

//Function if you have purched or can't
const buttonbuy = () => {

  if (balance >= selectedComputer.price) {
    alert("You have purched");

    balance -= selectedComputer.price;
  }
  else {

    alert("Expensive");

  }
  showBalance()
}
const buttonWork = () => {

  pay += 100;
  showBalance();
}
//Function sending money to the bank balance while deducting 10% and pay loan with it
const buttonbank = () => {

  if (loan > 0) {
    deductTen = pay * 0.1;
    deductNine = pay * 0.9;
    loan -= pay - deductNine;
    balance += pay - deductTen;

  }
  else {

    balance += pay;

  }
  pay = 0;
  showBalance();
}
//Funciton for showing  blance account and loan
const showBalance = () => {
  payElement.innerHTML = `pay ${pay.toFixed(2)}` + " kr";
  balanceElement.innerHTML = `Balance:${balance.toFixed(2)}` + " kr";
  loanBalanceElement.innerHTML = `Loan:${loan.toFixed(2)}` + " kr";

}

//Loaning money ,show that you cannot get a loan more than double of your bank balance and cannot get more than one bank loan before repaying the last loan.
const loanBank = () => {

  console.log(this);
  const requestLoan = prompt("please enter the amount of money you wish to loan: ");
  approvedLoan = parseFloat(requestLoan);
  if (loan !== 0) {
    alert("you have already loan");
  }
  else if (approvedLoan > balance * 2) {

    alert("To high loan");
  }
  else if (!loan) {    
    
    //Math.abs()
    
    loan += approvedLoan;
    
    repayButtonElement.style.display = "block";
    loanBalanceElement.style.display = "block";
    showBalance();
  }

}

//Function for repaying loan
const repayLoan = () => {


  if (pay < loan) {
    alert("Not enouagh to pay the loan");

  }

  else if (loan !== 0 || loan <= 0) {
    balanceDiffrence = pay - loan;;
    loan = balanceDiffrence - loan;
    balance = balance + balanceDiffrence;

    pay = 0;
    if (loan <= 0) {
      loan = 0;

      loanBalanceElement.style.display = "none";
      repayButtonElement.style.display = "none";
    }

  }

  showBalance();
}

loanButtonElement.addEventListener("click", loanBank)
addWorkButttonElement.addEventListener("click", buttonWork);
bankButtonElement.addEventListener("click", buttonbank);
repayButtonElement.addEventListener("click", repayLoan);
computersElement.addEventListener("change", handlerComputerChange);



